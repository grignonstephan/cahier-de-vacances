% author : sylcha
% licence : Creative Commons Attribution-NonCommercial-ShareAlike 4.0
%           https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
% purpose : génération d'une publication spécifique pour les énigmes du cahier de vacances Coopmaths
\NeedsTeXFormat{LaTeX2e}%
\ProvidesClass{enigme}[2024/06/29 v0.1]%
\RequirePackage{ifthen}
\newboolean{twocolumnstoc}
\DeclareOption{twocolumnstoc}{\setboolean{twocolumnstoc}{true}}
\newif\ifenigme@stapled
\DeclareOption{stapled}{\enigme@stapledtrue}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}%
\ProcessOptions\relax%
\RequirePackage{kvoptions} % key-value options pour passer titre, thème etc
\DeclareStringOption[Tle Spé]{niveau}
\newcommand{\Niveau}{\enigme@niveau}
\DeclareStringOption[Source Sans Pro]{mainfont}%
\DeclareStringOption[Source Code Pro]{monofont}%
\DeclareStringOption[GFS Neohellenic Math]{mathfont}%
\DeclareStringOption[Jellee]{titlefont}%
\ProcessKeyvalOptions*
\LoadClass[10pt,french,a4paper,twoside,openany]{book}%
\RequirePackage{enigme-commands}
\RequirePackage{ProfCollege}
\RequirePackage{ProfMaquette}
\RequirePackage[build]{ProfLycee}
\RequirePackage[french]{babel}
% \RequirePackage{fancyqr}
\RequirePackage{arydshln}% lignes en tirets dans un tableau
\RequirePackage{tabularx}
\RequirePackage{pgfplots}
\RequirePackage{tkz-tab}
\RequirePackage{tkz-euclide}
\RequirePackage{pas-tableur}
\RequirePackage{wrapfig}
\RequirePackage{svg}
\RequirePackage{pythonhighlight}
% geometry based on stapled option
\ifenigme@stapled%
  \RequirePackage[a4paper,%
    twoside,%
    bindingoffset=1cm,%
    inner=1.5cm,%
    outer=1.5cm,%
    top=1.5cm,%
    bottom=1.5cm]{geometry}%
\else%
  \RequirePackage[a4paper,%
    margin=1.5cm]{geometry}%
\fi
\RequirePackage{setspace}
\RequirePackage{hyperref}
\hypersetup{
  unicode=true,          % non-Latin characters in Acrobat’s bookmarks
  pdfencoding=auto,
  pdftoolbar=true,        % show Acrobat’s toolbar?
  pdfmenubar=true,        % show Acrobat’s menu?
  pdffitwindow=false,     % window fit to page when opened
  pdfstartview={FitH},    % fits the width of the page to the window
  pdftitle={Coopmaths -- Cahier de Vacances},    % title
  pdfauthor={Coopmaths},     % author
  pdfsubject={Cahier de vacances},   % subject of the document
  pdfcreator={},   % creator of the document
  pdfproducer={}, % producer of the document
  pdfkeywords={}, % list of keywords
  pdfnewwindow=true,      % links in new PDF window
  colorlinks=true,       % false: boxed links; true: colored links
  linkcolor=OrangeCoopmaths,          % color of internal links (change box color with linkbordercolor)
  citecolor=green,        % color of links to bibliography
  filecolor=magenta,      % color of file links
  urlcolor=OrangeCoopmaths           % color of external links
}
\RequirePackage[warnings-off={mathtools-colon,mathtools-overbracket}]{unicode-math}
\RequirePackage{pstricks}
\RequirePackage{titlesec}
\RequirePackage{titletoc}
\RequirePackage{microtype}
\RequirePackage{enumitem}
\RequirePackage{pst-fun,pst-all,pstricks-add,pst-eucl,pst-solides3d,pst-poly}
\RequirePackage{pstricks,pst-all,pst-plot,pstricks-add,pst-tree,pst-3dplot,pst-func}
\RequirePackage{frcursive}
\RequirePackage{bclogo}
\RequirePackage{qrcode}
\setlist[enumerate,1]{label=\arabic{*})}
\setlist[enumerate,2]{label=\alph{*})}
\graphicspath{{images/}}
% 
%                  Mise en page 
% 
% en-tête, pied de page
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}%clear all headers and footers
\fancyfoot[RO,LE]{\thepage}
\fancyfoot[C]{\textcolor{black!40}{\footnotesize Cahier de vacances Coopmaths}}
\renewcommand{\headrulewidth}{0mm}
\fancypagestyle{plain}{
  \fancyhf{}
  \fancyfoot[RO]{\thepage}
  \fancyfoot[LE]{\thepage}
  \fancyfoot[C]{\textcolor{black!40}{\footnotesize Cahier de vacances Coopmaths}}
  \renewcommand{\headrulewidth}{0pt}
}
\setlength{\parindent}{0pt}
% fontes
\setmainfont{\enigme@mainfont}
\setmonofont{\enigme@monofont}
\setmathfont{\enigme@mathfont}
\newfontface{\titlefont}{\enigme@titlefont}%
\newfontfamily\myfontScratch[]{FreeSans}
\defaultfontfeatures{Ligatures=TeX, Scale=MatchLowercase}%
\setstretch{1.2}
\setitemize[0]{leftmargin=*,label=\textbullet}
\setenumerate[0]{leftmargin=*}
\setlength\multicolsep{1pt}
\newcommand\hautab[1]{\renewcommand{\arraystretch}{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash}p{#1cm}}


% author : sylcha
% licence : Creative Commons Attribution-NonCommercial-ShareAlike 4.0
%           https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
% purpose : génération d'un cahier de vacances Coopmaths
\NeedsTeXFormat{LaTeX2e}%
\ProvidesClass{cdv}[2024/06/07 v0.1]%
\RequirePackage{ifthen}
\newboolean{twocolumnstoc}
\DeclareOption{twocolumnstoc}{\setboolean{twocolumnstoc}{true}}
\newboolean{terminale}
\DeclareOption{terminale}{\setboolean{terminale}{false}}
\newif\ifcdv@stapled
\DeclareOption{stapled}{\cdv@stapledtrue}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}%
\ProcessOptions\relax%
\RequirePackage{kvoptions} % key-value options pour passer titre, thème etc
\DeclareStringOption[Tle Spé]{niveau}
\newcommand{\Niveau}{\cdv@niveau}
\DeclareStringOption[Source Sans Pro]{mainfont}%
\DeclareStringOption[Source Code Pro]{monofont}%
\DeclareStringOption[GFS Neohellenic Math]{mathfont}%
\DeclareStringOption[Jellee]{titlefont}%
\ProcessKeyvalOptions*
\LoadClass[10pt,french,a4paper,twoside,openany]{book}%
\RequirePackage[level=\Niveau,terminale=\boolean{terminale}]{cdv}
\RequirePackage{cdv-commands}
\RequirePackage{ProfCollege}
\RequirePackage{ProfMaquette}
\RequirePackage[build]{ProfLycee}
\RequirePackage[french]{babel}
% \RequirePackage{fancyqr}
\RequirePackage{arydshln}% lignes en tirets dans un tableau
\RequirePackage{tabularx}
\RequirePackage{pgfplots}
\RequirePackage{tkz-tab}
\RequirePackage{tkz-euclide}
\RequirePackage{pas-tableur}
\RequirePackage{wrapfig}
\RequirePackage{svg}
\RequirePackage{pythonhighlight}
% geometry based on stapled option
\ifcdv@stapled%
  \RequirePackage[a4paper,%
    twoside,%
    bindingoffset=1cm,%
    inner=1.5cm,%
    outer=1.5cm,%
    top=1.5cm,%
    bottom=1.5cm]{geometry}%
\else%
  \RequirePackage[a4paper,%
    margin=1.5cm]{geometry}%
\fi
\RequirePackage{setspace}
\RequirePackage{hyperref}
\hypersetup{
  unicode=true,          % non-Latin characters in Acrobat’s bookmarks
  pdfencoding=auto,
  pdftoolbar=true,        % show Acrobat’s toolbar?
  pdfmenubar=true,        % show Acrobat’s menu?
  pdffitwindow=false,     % window fit to page when opened
  pdfstartview={FitH},    % fits the width of the page to the window
  pdftitle={Coopmaths -- Cahier de Vacances},    % title
  pdfauthor={Coopmaths},     % author
  pdfsubject={Cahier de vacances},   % subject of the document
  pdfcreator={},   % creator of the document
  pdfproducer={}, % producer of the document
  pdfkeywords={}, % list of keywords
  pdfnewwindow=true,      % links in new PDF window
  colorlinks=true,       % false: boxed links; true: colored links
  linkcolor=OrangeCoopmaths,          % color of internal links (change box color with linkbordercolor)
  citecolor=green,        % color of links to bibliography
  filecolor=magenta,      % color of file links
  urlcolor=OrangeCoopmaths           % color of external links
}
\RequirePackage[warnings-off={mathtools-colon,mathtools-overbracket}]{unicode-math}
\RequirePackage{pstricks}
\RequirePackage{titlesec}
\RequirePackage{titletoc}
\RequirePackage{microtype}
\RequirePackage{enumitem}
\RequirePackage{pst-fun,pst-all,pstricks-add,pst-eucl,pst-solides3d,pst-poly}
\RequirePackage{pstricks,pst-all,pst-plot,pstricks-add,pst-tree,pst-3dplot,pst-func}
\RequirePackage{frcursive}
\RequirePackage{bclogo}
\RequirePackage{qrcode}
\setlist[enumerate,1]{label=\arabic{*})}
\setlist[enumerate,2]{label=\alph{*})}
\graphicspath{{images/}}
% 
%                  Mise en page 
% 
% en-tête, pied de page
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}%clear all headers and footers
\fancyfoot[RO,LE]{\thepage}
\fancyfoot[C]{\textcolor{black!40}{\footnotesize Cahier de vacances Coopmaths}}
\renewcommand{\headrulewidth}{0mm}
\fancypagestyle{plain}{
  \fancyhf{}
  \fancyfoot[RO]{\thepage}
  \fancyfoot[LE]{\thepage}
  \fancyfoot[C]{\textcolor{black!40}{\footnotesize Cahier de vacances Coopmaths}}
  \renewcommand{\headrulewidth}{0pt}
}
\setlength{\parindent}{0pt}
% fontes
\setmainfont{\cdv@mainfont}
\setmonofont{\cdv@monofont}
\setmathfont{\cdv@mathfont}
\newfontface{\titlefont}{\cdv@titlefont}%
\newfontfamily\myfontScratch[]{FreeSans}
\defaultfontfeatures{Ligatures=TeX, Scale=MatchLowercase}%
\setstretch{1.2}
\setitemize[0]{leftmargin=*,label=\textbullet}
\setenumerate[0]{leftmargin=*}
\setlength\multicolsep{1pt}
\newcommand\hautab[1]{\renewcommand{\arraystretch}{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash}p{#1cm}}
% section
\makeatletter
\renewcommand{\thesection}{\@arabic\c@section}
\makeatother
\titlespacing*{\section}{0mm}{5mm}{3mm}
\titlespacing*{name=\section,numberless}{0mm}{5mm}{3mm}
\titleformat{\section}[hang]
{\LARGE\bf\sffamily\color{BleuCoopmaths}}
{\tcbox[colback=BleuCoopmaths,colframe=BleuCoopmaths,coltext=white,on line,boxsep=0pt,left=4pt,right=4pt,top=4pt,bottom=4pt,sharp corners,tcbox raise base]{\thesection}}
{10pt}
{}
% subsection
\renewcommand{\thesubsection}{\arabic{section}.\arabic{subsection}}
\makeatother
\titlespacing*{\subsection}{3mm}{3mm}{3mm}
\titleformat{\subsection}[hang]
{\large\bf\sffamily\color{BleuCoopmaths}}
{\tcbox[colback=white,colframe=white,coltext=BleuCoopmaths,on line,boxsep=0pt,left=2pt,right=5pt,top=2pt,bottom=2pt]{\thesubsection}}
{1pt}
{}
% Redéfinition du nom du chapitre
\addto{\captionsfrench}{\renewcommand*{\chaptername}{}}
\titleformat{\chapter}[display]{\normalfont\bfseries}{}{0pt}{\Huge}
\titlespacing*{\chapter}{0pt}{-50pt}{10pt}
% Environnements
\tcbset{frame/.style={enhanced,breakable, sharp corners, after skip=5mm,  toprule=1mm,rightrule=0pt,bottomrule=0pt,
      leftrule=0pt, colback=#1!10,
      colframe=#1!80!black, coltitle=#1!50!black,
      detach title,overlay unbroken and first={
          \node[anchor=north west,font=\sffamily,yshift=-2mm] at (frame.north west) {\tcbtitle}; % Adjust the yshift value
        }, before upper={\vspace{5mm}},}}
% ToC
% ToC sur deux colonnes
\ifthenelse{\boolean{twocolumnstoc}}{%
  \RequirePackage[toc]{multitoc}
  \setlength{\columnsep}{5mm}
}{}
%  espace avant (source: https://stackoverflow.com/a/64303513)
\makeatletter
\renewcommand\tableofcontents{%
  \if@twocolumn
    \@restonecoltrue\onecolumn
  \else
    \@restonecolfalse
  \fi
  \chapter*{\vspace{2cm}\contentsname
    \@mkboth{%
      \MakeUppercase\contentsname}{\MakeUppercase\contentsname}}%
  \@starttoc{toc}%
  \if@restonecol\twocolumn\fi
}
\makeatother
% Chapter
% \titlecontents{chapter}[0em]
% {\vskip 3ex}% above code
% {\Large \bfseries}% numbered sections formatting
% {\Large \bfseries}% unnumbered sections formatting
% {}%
% [\hrule\vskip 1ex]
% Section
% \titlecontents{section}[2em]
% {\vskip 0.5ex}% above code
% {}% numbered sections formatting
% {}% unnumbered sections formatting
% {\quad\titlerule*[0.5pc]{\textcolor{DarkGray}{.}}\contentspage}%
\RequirePackage{tikzpagenodes}
\AtBeginDocument{%
  \thispagestyle{empty}
  % \begin{tikzpicture}[remember picture,overlay]
  %   \node[inner sep=0pt] at ($(current page.south west)!0.5!(current page.north east)$){
  %     \includesvg[width=\paperwidth]{images/couv_coopmaths_cdv.svg}
  %   };
  % \end{tikzpicture}
  %   \noindent\makebox[\textwidth]{\includesvg[width=\paperwidth]{images/couv_coopmaths_cdv.svg}}
  %   \newpage
  \begin{tikzpicture}[remember picture,overlay]
    \node[inner sep=0pt] at ([xshift=7mm,yshift=-2mm]current page.center){\includesvg[width=1.1\paperwidth]{images/couv_coopmaths_cdv.svg}};
    \node[inner sep=0pt,above left=5mm of current page.south east,anchor=south east] {\Large\bfseries\sffamily\href{mailto:contact@coopmaths.fr}{contact@coopmaths.fr}};
    \coordinate (levelplace) at ([xshift=-3cm]$(current page.north east)!0.5!(current page.south east)$);
    \node[fill=OrangeCoopmaths,text=white,rounded corners,text width=4cm,align=center,rotate=30,inner sep=10pt,font={\Huge\bfseries\sffamily\linespread{0.8}\selectfont}] at (levelplace) {Vers la \Niveau};
  \end{tikzpicture}
  \clearpage
}
\AtEndDocument{%
  \clearpage
  \thispagestyle{empty}
  \begin{tikzpicture}[remember picture,overlay]
    \node[inner sep=0pt] at ([xshift=7mm,yshift=-2mm]current page.center){\includesvg[width=1.1\paperwidth,inkscapelatex=false]{images/couv_coopmaths_cdv_last.svg}};
  \end{tikzpicture}
}

